# Project: init

### *Info:* Basic introduction to the basic topics of DevOps at UNIT Factory.

**init** will give you the opportunity to discover system and network basic commands,  
many of the services used on a server machine, as well as a few ideas of scripts  
that can be useful for SysAdmins on a daily basis.

#### *Subject:* [init.pdf](https://bitbucket.org/vliubko/init/raw/fb8bb1a8a31d362a7aea598165b3998a008679c2/init.en.pdf "init.en.pdf")